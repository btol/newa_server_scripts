from pathlib import Path

import numpy as np
import xarray as xr
import zarr

from pathlib import Path

from netCDF4 import date2num
import pandas as pd

from newapy import wrf
from newapy import utils


path = Path(__file__).resolve().parents[1]
path_data = path / 'data' / 'wrf'
path_tmp  = path / 'tmp' / 'tab'
path_tab = Path('/newa/WP3/Mesoscale/Production/tabfiles/')
path_mask = Path('/home/btol/projects/newapy/data/wrf/')


# def int16_encoding_for_float(da):
    # vmin, vmax = da.min().compute().values, da.max().compute().values
    # offset = vmin + (vmax - vmin) / 2
    # scale = 1.1 * (vmax - vmin) / 65535
    # encoding = {'dtype': 'int16',
    #             'scale_factor': scale,
    #             'add_offset': offset,
    #             '_FillValue': 32767}
    # encoding = {'dtype': 'int16',
    #             'scale_factor': scale,
    #             'add_offset': offset,
    #             '_FillValue': 32767}
    # return encoding


@utils.timeit
def save_netcdf(ds, outfile, complevel=1):
    enc = {var: {'zlib': True, 'complevel': complevel} for var in ds.data_vars}
    ds.to_netcdf(outfile, encoding=enc)


@utils.timeit
def prepare_tab_domain_height(dom='CE', height=100):

    file_name = f'TabFile_{dom}_{height:03d}.nc'
    tab_file = path_tab / file_name
    tab = xr.open_dataset(tab_file)
    tab.load()
    tab.close()

    mask = xr.open_dataset(path_mask / f'{dom}_mask.d03.nc')
    mask.load()
    mask.close()

    mask = mask.rename({'XLONG_M': 'XLON',
                        'XLAT_M': 'XLAT',
                        'Time': 'time'})
    mask = mask.squeeze(drop=True)

    ds = mask.update(tab)
    ds = wrf.add_dimcoords(ds)

    ds = ds.isel({'west_east': slice(15, -15),
                  'south_north': slice(15, -15)})

    ds['FREQ'] = ds['FREQ'].expand_dims({'height': [float(height)]})

    ds = ds.assign_coords(
        sector=(('sector',), np.linspace(0.0, 350.0, 36)),
        sector_bins=(('sector_bins',), np.linspace(-5.0, 355.0, 37)),
        wind=(('wind',), np.linspace(0.5, 29.5, 30)),
        wind_bins=(('wind_bins',), np.linspace(0.0, 30.0, 31)),
        stab=(('stab',), [16.45, 125, 350, 10000, -350, -150, -37.85]),
        west_east=(('west_east',), np.round(ds.west_east.values, -1)),
        south_north=(('south_north',), np.round(ds.south_north.values, -1)),
    )

    ds = ds.drop(['wdirCl', 'wspdCl', 'Ltypical', 'WMAXS', 'LEV'])

    ds['FREQ'] = ds['FREQ'].where(ds['NEWA_MASK']==1)
    ds.load()
    # mask.close()
    # tab.close()

    # datasets.append(ds)

    # print(len(datasets))
    # if len(datasets) == 0:
    #     return None

    # # combined = xr.combine_by_coords(datasets, combine_attrs='override')
    # combined = wrf.merge_newa_datasets(datasets)

    # attrs = ds.attrs
    # for var in ['CEN_LAT', 'CEN_LON']:
    #     del attrs[var]

    # attrs['WEST-EAST_GRID_DIMENSION'] = combined.dims['west_east']
    # attrs['SOUTH-NORTH_GRID_DIMENSION'] = combined.dims['south_north']

    # combined.attrs.update(attrs)
    # combined['NEWA_DOMC'].attrs['NEWA_DOM_CODES'] = mask.attrs['NEWA_DOM_CODES']

    # llcoords = {'XLAT': (('south_north', 'west_east'), combined['XLAT']),
    #             'XLON': (('south_north', 'west_east'), combined['XLON'])}
    # combined = combined.assign_coords(**llcoords)

    # for ds in datasets:
    #     ds.close()

    return ds


if __name__ == '__main__':
    domains = [
        'BA',
        'CE',
        'FR',
        'GB',
        'GR',
        'IB',
        'IT',
        'SA',
        'SB',
        'TR'
    ]
    heights = [
        50,
        75,
        100,
        150,
        # 200
    ]

    for height in heights:

        for dom in domains:

            print(dom, height)
            # zarrfile = path_tmp / f'tab_{dom}_{height}.zarr'
            ncfile =  path_tmp / f'tab_{dom}_{height}_35.nc'

            if ncfile.is_file():
                continue

            ds = prepare_tab_domain_height(dom, height)

            for isec in range(36):
                ncfile =  path_tmp / f'tab_{dom}_{height}_{isec}.nc'

                if ncfile.is_file():
                    continue

                sub = ds.isel(sector=slice(isec, isec+1))
                # sub.attrs = {}

                # sub = sub.drop([
                    # 'XLON',
                    # 'XLAT',
                    # 'NEWA_MASK',
                    # 'NEWA_DOMC',
                    # 'wind_bins',
                    # 'sector_bins',
                # ])
                # print(sub['FREQ'].values)
                # print(sub)
                # print(sub['FREQ'].attrs)
                sub['FREQ'] = sub['FREQ'].where(~np.isnan(sub['FREQ']), -9999).astype(np.int32)
                # print(sub)

                sub.load()
                sub.to_netcdf(ncfile)
                sub.close()

            ds.close()

        # merged = merged.chunk({'west_east': 100, 'south_north': 100})

        # compressor = zarr.Blosc(cname='zstd', clevel=3, shuffle=2)
        # encoding = {
        #     'FREQ': {
        #         # 'dtype': 'int16',
        #         # 'scale_factor': 2.0,
        #         # 'add_offset': 32766,
        #         # '_FillValue': -32768
        #     }
        # }

        # merged.to_netcdf(
        #     ncfile,
        #     encoding=encoding
        # )

        # encoding['FREQ']['compressor'] = compressor

        # # ds = add_crs(ds, 'EPSG:3763')
        # # ds['elev'] = add_crs(ds['elev'], 'EPSG:3763')

        # merged.to_zarr(
        #     zarrfile,
        #     encoding=encoding
        # )

        # # save_netcdf(merged, '/newa/WP3/Mesoscale/Production/corr_factors/corr_merged.nc')



