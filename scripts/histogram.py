import numpy as np
import xarray as xr


def wind_histogram(ws, wd, dim='time', n_ws=30, n_wd=12):

    ws_bins = np.linspace(0.0, float(n_ws), n_ws+1)
    ws_bin_centers = 0.5 * (ws_bins[:-1] + ws_bins[1:])

    dtheta = 360.0 / n_wd
    wd_bins = np.linspace(dtheta / 2.0, 360.0 - dtheta / 2.0, n_wd)
    wd_bins = np.concatenate([[0.0], wd_bins, [360.0]])
    wd_bin_centers = np.linspace(0.0, 360.0 - dtheta, n_wd)

    def _hist(ws, wd):
        hist, _, _ = np.histogram2d(ws, wd, bins=(ws_bins, wd_bins))
        hist[:, 0] = hist[:, -1]
        hist = hist[:, :-1]
        return hist

    input_core_dims = [[dim], [dim]]
    output_core_dims = [['ws_bin', 'wd_bin']]

    hist = xr.apply_ufunc(
        _hist,
        ws, wd,
        input_core_dims=input_core_dims,
        output_core_dims=output_core_dims,
        vectorize=True,
        dask='allowed',
    )

    hist = hist.assign_coords(
        ws_bin=(('ws_bin'), ws_bin_centers),
        wd_bin=(('wd_bin'), wd_bin_centers),
    )
    hist.name = 'wind_hist'

    return hist


def hist_moment(hist, *, edges=None, moment=1.0, axis=None):
    '''
    Parameters
    ----------
    moment : float, int
        Moment to return

    sectors : boolean
        Returns by sector if True

    nws : int
        Number of wind speed bins (of size 1 m/s) to use.

    Returns
    -------
    ws_moment : float
        Wind speed moment
    ws_moment : numpy.ndarray
        Wind speed moment by sector if sectors=True
    '''
    if edges is None:
        n_ws = hist.shape[0 if axis is None else axis]
        edges = np.linspace(0.0, float(n_ws), n_ws+1)
    centers = 0.5 * (edges[1:] + edges[:-1])
    hist = hist / np.sum(hist, axis=axis)
    return np.sum(hist.T * centers**moment, axis=axis)


def ws_prob_gt_mean(hist, *, edges=None, axis=None):
    '''
    Parameters
    ----------
    ws_hist : numpy.ndarray
        Wind speed distribution

    Returns
    -------
    Skewness : float
    '''

    def _prob_lt_mean(hist, edges):
        centers = 0.5 * (edges[:-1] + edges[1:])
        hist = hist / np.sum(hist)
        mean = np.sum(centers * hist)
        cdf = np.cumsum(hist)
        ceils = edges[1:]
        return 1.0 - np.interp(mean, ceils, cdf)

    if edges is None:
        n_ws = hist.shape[0 if axis is None else axis]
        edges = np.linspace(0.0, float(n_ws), n_ws+1)

    if hist.ndim > 1 and axis is None:
        raise ValueError('axis must be specified when hist.ndim > 1')
    elif hist.ndim == 1 and axis is None:
        return _prob_lt_mean(hist, edges)
    else:
        return np.apply_along_axis(_prob_lt_mean, axis, hist, edges)
