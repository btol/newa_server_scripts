import math

import numpy as np
import numba
import xarray as xr

from histogram import hist_moment, ws_prob_gt_mean


@numba.jit(nopython=True, nogil=True)
def _calc_fit_value(CNST, DEN, C):
    value = CNST + math.lgamma(1.0 + C) - DEN * C
    return value


@numba.jit(nopython=True, nogil=True)
def fit03(ws_1st_moment, ws_3rd_moment, ws_skewness):
    '''
    WAsP-like weibull fit using the 1st and 3rd moment of wind speed, and the
    skewness of the wind speed distribution.

    Parameters
    ----------
    ws_1st_moment : float
        First moment of the wind speed distribution

    ws_3rd_moment : float
        Third moment of the wind speed distribution

    ws_skewness : float
        Skewness of the wind speed distribution

    Returns
    -------
    A : float
        Weibull A-parameter ('Magnitude-parameter')

    k : float
        Weibull k-parameter ('Shape-parameter')
    '''

    DEN = np.log(-np.log(ws_skewness))
    CNST = -(np.log(ws_3rd_moment) - 3.0*np.log(ws_1st_moment))

    # Factor used to reduce/increase CL, CH, and CC in the effort to
    # approximate value
    FACT = 2.0

    # initial values of constants cl, ch, cc
    CL = 3./2.0
    CH = 3./2.0
    CC = 3./2.0

    # The goal of step one is to get two constants (C1, C2) that correspond to
    # values on both 'sides' of 0 (i.e. one negative and one positive).
    value = _calc_fit_value(CNST, DEN, CH)

    # ABS
    abs_value = np.abs(value)

    # SI, SL, and SH are used to store the sign of value at a given time.
    SI = np.sign(value)
    SL = np.sign(value)
    SH = np.sign(value)

    CMAX = 30
    # The while loop below keeps iterating the constants CL, CH, CC
    # Until two of the constants represent value's that are negative and
    # positive respectively. Then those constants are used in the next
    # procedure.
    while ((SI == SL) & (SH == SI)):
        CI = CL
        CL = CI / FACT

        value = _calc_fit_value(CNST, DEN, CL)

        if (np.abs(value) < abs_value):
            abs_value = np.abs(value)
            CC = CL

        SL = np.sign(value)

        if (SL != SI):
            C1 = CI
            C2 = CL
            break

        CI = CH
        CH = CH*FACT

        if CH > CMAX:
            CI = CC
            k = 3.0 / CI
            A = (ws_3rd_moment / math.gamma(1.0 + CI))**(1.0/3.0)
            return A, k

        value = _calc_fit_value(CNST, DEN, CH)

        if (np.abs(value) < abs_value):
            abs_value = np.abs(value)
            CC = CH

        SH = np.sign(value)

        if (SH != SI):
            C1 = CI
            C2 = CH

    # The procedure below lets the constants C1 and C2 approach each other
    # until the difference between them is small enough. Then the constant
    # that is found is used to derive k and A.
    diff = np.abs(C2 - C1)
    while diff > 0.003:

        CI = 0.5 * (C1 + C2)
        value = _calc_fit_value(CNST, DEN, CI)

        if (np.sign(value) != SI):
            C2 = CI
        else:
            C1 = CI

        diff = np.abs(C2 - C1)

    k = 3.0 / CI
    A = (ws_3rd_moment / math.gamma(1.0 + CI))**(1.0/3.0)

    return A, k


def fit_weibull(hist, dim='ws_bin'):

    def _fit(x):
        U1 = hist_moment(x, moment=1.0)
        U3 = hist_moment(x, moment=3.0)
        PROB_LT_U1 = ws_prob_gt_mean(x)
        A, k = fit03(U1, U3, PROB_LT_U1)
        return A, k

    dens = hist / hist.sum(dim=dim)

    weib_A, weib_k = xr.apply_ufunc(
        _fit,
        dens,
        input_core_dims=[[dim]],
        output_core_dims=[[], []],
        vectorize=True,
        dask='allowed',
    )

    weib_A.name = 'A'
    weib_k.name = 'k'

    return xr.merge([weib_A, weib_k])
