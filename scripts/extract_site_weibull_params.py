from pathlib import Path

import numpy as np
import pandas as pd
import xarray as xr

from histogram import wind_histogram
from weibull import fit_weibull

TMPDIR = Path(__file__).resolve().parents[1] / 'tmp'

path = Path('/newa/WP3/Mesoscale/Production/time_series/')
files = sorted(path.glob('P-SB-*_SB.nc'))
for f in files:
    print(f)

def preprocess(ds):
    times = []
    for x in ds['Times'].values:
        times.append(pd.to_datetime(''.join(s.decode() for s in x),
                                    format='%Y-%m-%d_%H:%M:%S'))

    ds = ds.assign_coords(
        time=(('time',), times)
    )
    return dsc

ds = xr.open_mfdataset(files, preprocess=preprocess, decode_cf=False)
print(ds)

hist_total = wind_histogram(ds['WS'], ds['WD'], n_wd=1)
weibull_total = fit_weibull(hist_total.squeeze(drop=True))
weibull_total = weibull_total.rename({'A': 'A_tot', 'k': 'k_tot'})

hist = wind_histogram(ds['WS'], ds['WD'], n_wd=12)
weibull = fit_weibull(hist)

merged = xr.merge([weibull, weibull_total])

merged = merged.rename({'wd_bin': 'sector'})

print(merged)

for isec in range(merged.dims['sector']):
    merged[f'A_{isec+1}'] = merged['A'].isel(sector=isec, drop=True)
    merged[f'k_{isec+1}'] = merged['k'].isel(sector=isec, drop=True)

merged = merged.drop(['A', 'k', 'sector'])


df = merged.to_dataframe()
df = df.reset_index()
df.to_csv(TMPDIR / 'NEWA_MESO_2000-2018_A_and_k.csv', index=False)
print(df)
