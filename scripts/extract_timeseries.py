'''

To run this script create a conda environment with the following packages.
 - dask
 - pandas
 - netcdf4
 - numpy
 - xarray
 - scipy
 - pyproj

Usage:
    python extract_timeseries.py

Best practices:
    - Adding dask workers speeds it up by approx. 0-30% per doubling.
    - Adding "parallel" flag to open_mfdataset does not seem to speed up
      the script, probably because preprocessing is not heavy work
    - Using nearest-neightbour interpolation is not much faster than
      linear interpolation. Possibly because most time is spend on IO (???)
    - Extract shorter time periods first, e.g. months or years. and then
      combine afterwards.

'''
from pathlib import Path

from pyproj import Proj, transform
import numpy as np
import pandas as pd
import xarray as xr
from distributed import Client, LocalCluster, progress
# from dask.diagnostics import ProgressBar

PATH_MESOSCALE_DATA = Path('/newa/WP3/PRODUCTION_RUNS/')


def set_num_threads(nt):
    import os
    nt = str(nt)
    os.environ['OPENBLAS_NUM_THREADS'] = nt
    os.environ['NUMEXPR_NUM_THREADS'] = nt
    os.environ['OMP_NUM_THREADS'] = nt
    os.environ['MKL_NUM_THREADS'] = nt


def preprocess(ds):
    ds = ds.drop(['LANDMASK', 'LU_INDEX'])
    return ds


def files_in_period(start, end, domain):
    start = pd.to_datetime(start)
    end = pd.to_datetime(end)
    if domain == 'MERGED':
        files = sorted(PATH_MESOSCALE_DATA.glob(f'{domain}/????/NEWA-????-??-??.nc'))
    else:
        files = sorted(PATH_MESOSCALE_DATA.glob(f'{domain}/????/P-{domain}-????-????-??-??.nc'))
    times = [pd.to_datetime(f.name[-13:-3], format='%Y-%m-%d') for f in files]
    files = [f for f, t in zip(files, times) if t >= start and t <= end]
    return files


def reproj_to_wrfproj(x, y):

    proj_ll = Proj(proj='latlong', datum='WGS84')
    proj_wrf = Proj(proj='lcc', lat_1=30.0, lat_2=60.0, lat_0=54.0,
                    lon_0=15.0, a=6370000, b=6370000)

    return transform(proj_ll, proj_wrf, x, y)


def interp_to_points(ds, x, y, *, names=None, method='nearest'):

    if method == 'exact':
        method = None

    x = xr.DataArray(x, dims='pt')
    y = xr.DataArray(y, dims='pt')

    def _interp_nearest(ds, x, y):
        coords = ds[['west_east', 'south_north']]
        coords = coords.assign_coords(we_indx=(('west_east',), np.arange(ds.dims['west_east'])))
        coords = coords.assign_coords(sn_indx=(('south_north',), np.arange(ds.dims['south_north'])))
        coords_sel = coords.sel(west_east=x, south_north=y, method='nearest')

        we_indx = xr.DataArray(coords_sel.coords['we_indx'], dims='pt')
        sn_indx = xr.DataArray(coords_sel.coords['sn_indx'], dims='pt')

        return ds.isel(west_east=we_indx, south_north=sn_indx)

    def _interp_linear(ds, x, y):
        if all(v in ds.data_vars for v in ['WS', 'WD']):
            U, V = (-np.abs(ds['WS'])*np.sin(np.pi/180.0 * ds['WD']),
                    -np.abs(ds['WS'])*np.cos(np.pi/180.0 * ds['WD']))
            ds['U'] = U
            ds['V'] = V
            ds = ds.drop(['WS', 'WD'])

        ds = ds.interp(west_east=x, south_north=y, method='linear')

        if all(v in ds.data_vars for v in ['U', 'V']):
            WS, WD = (np.sqrt(ds['U']**2 + ds['V']**2),
                      180.0 + np.arctan2(ds['U'], ds['V'])*180.0/np.pi)
            ds['WS'] = WS
            ds['WD'] = WD
            ds = ds.drop(['U', 'V'])

        return ds


    if method == 'nearest':
        ds = _interp_nearest(ds, x, y)
    elif method == 'linear':
        ds = _interp_linear(ds, x, y)
    else:
        ds = ds.interp(west_east=x, south_north=y, method=method)

    if names is not None:
        ds = ds.assign_coords(pt=(('pt'), names))

    return ds


def interp_to_heights(ds, heights, method='exact'):
    if method == 'exact':
        method = None
    return ds.sel(height=heights, method=method)


def extract_timeseries(domain='MERGED',
                       variables=['WS'],
                       points=[(12.0, 56.0)],
                       points_method='nearest',
                       heights=None,
                       heights_method='exact',
                       time_start='2015-01-01 00:00:00',
                       time_end='2015-01-05 00:00:00',
                       output_path='test.nc'):

    files = files_in_period(time_start, time_end, domain)
    ds = xr.open_mfdataset(files,
                           preprocess=preprocess,
                           # parallel=True,
                           compat='override',
                           coords='minimal',
                           data_vars='minimal')
    ds = ds[variables]
    # print(ds)

    ds = ds.sel(time=slice(time_start, time_end))

    x, y = reproj_to_wrfproj(np.array([pt[0] for pt in points]),
                             np.array([pt[1] for pt in points]))
    names = [pt[2] for pt in points]

    ds = interp_to_points(ds, x, y, names=names, method=points_method)

    if heights is not None:
        ds = interp_to_heights(ds, heights, method=heights_method)

    delayed_obj = ds.to_netcdf(output_path,
                               compute=False)

    delayed_obj = delayed_obj.persist()
    progress(delayed_obj)
    result = delayed_obj.compute()


if __name__ == '__main__':
    set_num_threads(1)
    cluster = LocalCluster(n_workers=4,
                           threads_per_worker=2,
                           memory_limit='6GB')
    client = Client(cluster)
    print(client)

    # Select mesoscale domain
    # domain = 'MERGED'
    # domain = 'FR'
    # domain = 'IB'
    domain = 'IT'
    # domain = 'SB'

    # Select variables
    variables = [
        # 'ABLAT_CYL',
        # 'ACCRE_CYL',
        # 'ALPHA',
        # 'HFX',
        # 'HGT',
        # 'LH',
        # 'PBLH',
        # 'WS',
        # 'WD',
        # 'PD',
        # 'PRECIP',
        # 'PSFC',
        # 'Q2',
        # 'QVAPOR',
        # 'RHO',
        # 'RMOL',
        # 'SEAICE',
        # 'SWDDIR',
        # 'SWDDNI',
        # 'T',
        'T2',
        # 'TKE',
        'TSK',
        # 'UST',
        'WD10',
        'WS10',
        # 'ZNT',
        # 'NEWA_DOMC',  # Only works for MERGED domain
        # 'NEWA_MASK',  # Only works for MERGED domain
        'crs',
        'Times'
    ]

    # Select points and interpolation method
    # Assumes longitude, latitude points
    # (x, y, name)
    # points = [
    #     (8.150833, 56.440556, "hovsore"),
    #     (7.15836, 55.19501, "fino3")
    # ]

    # points = [
    #     (9.79156, 57.32652, 'site1'),
    # ]

    points = [
        (16.23642, 38.88996, 'site1'),
        (15.92468, 38.8921, 'site2'),
    ]

    # points = [
    #     (8.165559, 56.4196, 'site1'),
    #     (7.21802, 56.44428, 'site2'),
    # ]

    # points = [
    #     (8.1958, 56.49283, 'site1'),
    #     (6.63025, 56.50799, 'site2'),
    # ]

    # points = [
    #     (-1.55457, 47.21957, 'pt'),
    # ]

    # points = [
    #     (8.81104, 57.53942, 'site1'),
    #     (6.72363, 56.18225, 'site2'),
    #     (6.3501,  54.09806, 'site3'),
    #     (2.52686, 52.52291, 'site4'),
    #     (9.59106, 57.13624, 'site5'),
    #     (8.26172, 56.21892, 'site6'),
    #     (6.57532, 53.36694, 'site7'),
    #     (1.47217, 52.60305, 'site8'),
    # ]

        # (15.80933, 38.93591, 'site'),

    # points = [
    #     (-1.55778, 42.69524, "alex17_MP5"),
    #     (-1.57235, 42.71566, "alex17_M2"),
    #     (-1.56337, 42.74385, "alex17_M3"),
    #     (-1.57139, 42.75001, "alex17_M6"),
    #     (-1.57664, 42.72496, "alex17_M7"),
    #     (-1.63025, 42.79228, "alex17_WLS70"),
    # ]

    points_method = 'nearest'  # "exact", "nearest", or "linear"

    # Select heights and interpolation method. Set to None for all heights
    # heights = [50.0, 75.0, 100.0, 150.0]  # meters AGL.
    heights = None  # meters AGL.
    heights_method = 'exact'   # "exact", "nearest", or "linear"

    # Select time period in 'YYYY-MM-DD hh:mm:ss' format
    year = '2008'
    time_start = f'{year}-01-01 00:00:00'
    # time_end = '2015-12-31 23:30:00'
    time_end = f'{year}-12-31 23:30:00'

    output_path = f'/home/btol/projects/newa_server_scripts/tmp/gabriele_it_{year}.nc'

    extract_timeseries(
        domain=domain,
        variables=variables,
        points=points,
        points_method=points_method,
        heights=heights,
        heights_method=heights_method,
        time_start=time_start,
        time_end=time_end,
        output_path=output_path
    )

    client.shutdown()
