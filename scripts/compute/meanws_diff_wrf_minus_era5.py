import numpy as np
import xarray as xr
from pyproj import Proj, transform

def make_longitude_plusminus(ds):
    longitude = ds.coords['longitude'].values
    longitude[longitude > 180.0] = longitude[longitude > 180.0] - 360.0
    ds = ds.assign_coords(longitude=("longitude", longitude))
    ds = ds.sortby(['longitude', 'latitude'])
    return ds


def diff_era5_wrf(era5, wrf):

    era5 = make_longitude_plusminus(era5)

    era5 = era5.sel(
        latitude=slice(25.0, 75.0),
        longitude=slice(-20.0, 75.0)
    )

    proj_wrf = Proj(proj='lcc',
                    lat_1=wrf.TRUELAT1, lat_2=wrf.TRUELAT2,
                    lat_0=wrf.MOAD_CEN_LAT, lon_0=wrf.STAND_LON,
                    a=6370000, b=6370000)

    proj_ll = Proj(proj='latlong', datum='WGS84')

    x, y = wrf['west_east'].values, wrf['south_north'].values
    X, Y = np.meshgrid(x, y)

    lons, lats = transform(proj_wrf, proj_ll, X, Y)

    lons = xr.DataArray(lons, dims=('south_north', 'west_east'))
    lats = xr.DataArray(lats, dims=('south_north', 'west_east'))
    era5 = era5.interp(latitude=lats, longitude=lons)
    era5.assign_coords(
        west_east=wrf['west_east'],
        south_north=wrf['south_north'],
        XLAT=wrf['XLAT'],
        XLON=wrf['XLON']
    )
    diff = wrf['MEAN_WS'] - era5
    return diff


def main():

    era5 = xr.open_dataset('/home/btol/projects/newa_server_scripts/tmp/era5_meanws.nc')
    print(era5)

    wrf = xr.open_dataset('/newa/WP3/Mesoscale/Production/mean_winds/Mean_all.nc')
    # wrf.attrs = {}
    print(wrf)

    dwrf = diff_era5_wrf(era5, wrf)
    dwrf.to_netcdf('/home/btol/projects/newa_server_scripts/tmp/meanwsdiff_wrf_minus_era5.nc')

    # wasp = xr.open_rasterio('/newa/web_data/maps/micro/ltm/wspd_hgt100m/micro_ltm_wspd_hgt100m.vrt')
    # print(wasp)

if __name__ == '__main__':
    main()
