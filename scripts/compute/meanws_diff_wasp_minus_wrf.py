from pathlib import Path

import numpy as np
import xarray as xr
from pyproj import Proj, transform


def diff_wasp_wrf(wasp, wrf):

    # wasp = wasp.isel(x=slice(None, None, 10),
    #                  y=slice(None, None, 10))

    proj_wasp = Proj(init='epsg:3035')

    proj_wrf = Proj(proj='lcc',
                    lat_1=wrf.TRUELAT1, lat_2=wrf.TRUELAT2,
                    lat_0=wrf.MOAD_CEN_LAT, lon_0=wrf.STAND_LON,
                    a=6370000, b=6370000)

    x, y = wasp['x'].values, wasp['y'].values
    X, Y = np.meshgrid(x, y)

    X_wrf, Y_wrf = transform(proj_wasp, proj_wrf, X, Y)

    X_wrf = xr.DataArray(X_wrf, dims=('y', 'x'))
    Y_wrf = xr.DataArray(Y_wrf, dims=('y', 'x'))

    wrf = wrf.interp(west_east=X_wrf, south_north=Y_wrf)

    wrf = wrf.assign_coords(
        x=wasp['x'],
        y=wasp['y'],
    )

    diff = wasp['WS'] - wrf['MEAN_WS']
    diff.name = 'WS'

    return diff


def main():
    wrf = xr.open_dataset('/newa/WP3/Mesoscale/Production/mean_winds/Mean_all.nc')
    wrf = wrf[['MEAN_WS']]

    wasp_subset_file = Path('/home/btol/projects/newa_server_scripts/tmp/wasp_subset.nc')
    if wasp_subset_file.is_file():
        wasp = xr.open_dataset(wasp_subset_file)
        wasp = wasp.sortby('y')
        wasp = wasp.squeeze(drop=True)
    else:
        tmp = xr.open_rasterio('/newa/web_data/maps/micro/ltm/wspd_hgt100m/micro_ltm_wspd_hgt100m.vrt')
        tmp = tmp.chunk({'y': 1000, 'x': 1000})
        wasp = tmp.isel(x=slice(None, None, 10), y=slice(None, None, 10))
        wasp.name = 'WS'
        wasp = wasp.to_dataset()
        enc = {'WS': {'dtype': 'int16',
                      'scale_factor': 0.0005,
                      'add_offset': 20.0,
                      '_FillValue': -32768}}
        wasp.to_netcdf(wasp_subset_file, encoding=enc)

    diff = diff_wasp_wrf(wasp, wrf)
    # diff.attrs = wasp.attrs
    # diff.name = 'WS'
    diff.attrs = {}
    diff = diff.to_dataset()
    # diff['WS'].attrs['nodatavals'] = 'NaN'
    print(diff)
    enc = {'WS': {'dtype': 'int16',
                'scale_factor': 0.005,
                '_FillValue': -32768}}
    diff.to_netcdf('/home/btol/projects/newa_server_scripts/tmp/meanwsdiff_wasp_minus_wrf.nc', encoding=enc)


if __name__ == '__main__':
    main()
