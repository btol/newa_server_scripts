from pathlib import Path 

import numpy as np
import xarray as xr 
import zarr 

from pathlib import Path

from netCDF4 import date2num
import pandas as pd

from newapy import wrf
from newapy import utils


path = Path(__file__).resolve().parents[1]
path_data = path / 'data' / 'wrf'
path_tmp  = path / 'tmp' / 'tab'
path_tab = Path('/newa/WP3/Mesoscale/Production/tabfiles/')
path_mask = Path('/home/btol/projects/newapy/data/wrf/')


# def int16_encoding_for_float(da):
    # vmin, vmax = da.min().compute().values, da.max().compute().values
    # offset = vmin + (vmax - vmin) / 2
    # scale = 1.1 * (vmax - vmin) / 65535
    # encoding = {'dtype': 'int16',
    #             'scale_factor': scale,
    #             'add_offset': offset,
    #             '_FillValue': 32767}
    # encoding = {'dtype': 'int16',
    #             'scale_factor': scale,
    #             'add_offset': offset,
    #             '_FillValue': 32767}
    # return encoding


@utils.timeit
def save_netcdf(ds, outfile, complevel=1):
    enc = {var: {'zlib': True, 'complevel': complevel} for var in ds.data_vars}
    ds.to_netcdf(outfile, encoding=enc)


@utils.timeit
def prepare_tab_stage2(height=50, isec=0):
    datasets = []
    domains = ['BA', 'CE', 'FR', 'GB', 'GR', 'IB', 'IT', 'SA', 'SB', 'TR']
    for dom in domains:
        file_name = f'tab_{dom}_{height}_{isec}.nc'
        tab_file = path_tmp / file_name
        tab = xr.open_dataset(tab_file)
        tab.load()
        tab['FREQ'] = tab['FREQ'].where(tab['FREQ'] != -9999)
        datasets.append(tab)
        tab.close()

    if len(datasets) == 0:
        return None

    # combined = xr.combine_by_coords(datasets, combine_attrs='override')
    combined = wrf.merge_newa_datasets(datasets)
    combined.load()

    attrs = tab.attrs
    for var in ['CEN_LAT', 'CEN_LON']:
        del attrs[var]

    attrs['WEST-EAST_GRID_DIMENSION'] = combined.dims['west_east']
    attrs['SOUTH-NORTH_GRID_DIMENSION'] = combined.dims['south_north']

    combined.attrs.update(attrs)
    combined['NEWA_DOMC'].attrs['NEWA_DOM_CODES'] = "NaN: 0; BA: 1; CE: 2; FR: 3; GB: 4; GR: 5; IB: 6; IT: 7; SA: 8; SB: 9; TR: 10"

    llcoords = {'XLAT': (('south_north', 'west_east'), combined['XLAT']),
                'XLON': (('south_north', 'west_east'), combined['XLON'])}
    combined = combined.assign_coords(**llcoords)

    return combined


if __name__ == '__main__':

    heights = [
        # 50, 
        # 75,
        # 100, 
        # 150, 
        200
    ]

    for height in heights:
        for isec in range(36):

            ncfile =  path_tmp / f'tab_{height}_{isec}.nc'
            if ncfile.is_file():
                continue 

            ds = prepare_tab_stage2(height, isec)
            print(ds)
            ds.attrs = {}
            ds.to_netcdf(ncfile)    
            ds.close()

        # for height in heights:

        #     # zarrfile = path_tmp / f'tab_{dom}_{height}.zarr'

        #     ds = prepare_tab_domain_height(dom, height)
            
        #     for isec in range(36):
        #         ncfile =  path_tmp / f'tab_{dom}_{height}_{isec}.nc'
        #         ds.isel(sector=slice(isec, isec+1)).to_netcdf(ncfile)

        # merged = merged.chunk({'west_east': 100, 'south_north': 100})

        # compressor = zarr.Blosc(cname='zstd', clevel=3, shuffle=2)
        # encoding = {
        #     'FREQ': {
        #         # 'dtype': 'int16',
        #         # 'scale_factor': 2.0,
        #         # 'add_offset': 32766,
        #         # '_FillValue': -32768
        #     }
        # }

        # merged.to_netcdf(
        #     ncfile, 
        #     encoding=encoding
        # )

        # encoding['FREQ']['compressor'] = compressor 
        
        # # ds = add_crs(ds, 'EPSG:3763')
        # # ds['elev'] = add_crs(ds['elev'], 'EPSG:3763')
        
        # merged.to_zarr(
        #     zarrfile, 
        #     encoding=encoding
        # )

        # # save_netcdf(merged, '/newa/WP3/Mesoscale/Production/corr_factors/corr_merged.nc')



